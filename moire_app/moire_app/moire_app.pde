// Moire pattern generator
// Tomasz Sulej, generateme.blog@gmail.com, http://generateme.tumblr.com
// Licence: http://unlicense.org/

// Usage:
//   * press SPACE to save
//   * click to random change
//   * press 'a' to animate (and save frames)

import java.util.List;
import java.util.Arrays;
import controlP5.*;
import processing.serial.*;
import java.util.*;

import javax.swing.JColorChooser;
import java.awt.Color;
import javax.swing.JPanel;


// set up filename
String filename = "moire";
String fileext = ".jpg";
String foldername = "./";

List<String> line_colorlist = new ArrayList<String>();
List<String> bg_colorlist = new ArrayList<String>();
List<String> selected_type= new ArrayList<String>();


int image_size = 600; // setup result size
float stripe_density = 100 ; // higher number, narrower stripes 

float rx = 0.5; // operating range for x from -rx to rx, from 0.5 to 5
float ry = 0.5; // operating range for x from -ry to ry, from 0.5 to 5

boolean animate = false;
int frames = 24;

// working buffer
PGraphics buffer;

String sessionid;


//for slider
ControlP5 slider;
float sliderValue;

//for color button
ControlP5 Button_line;
ControlP5 Button_back;


//for downlist
//DropdownList dl;
String[] tokens;
int myCurrentIndex = 0;





void setup() {
  //surface.setSize(700,700);
  background(255,255,255);
  
  size(700,800);
  //surface.setTitle("Hello Moire");
  sessionid = hex((int)random(0xffff), 5);
  buffer = createGraphics(image_size, image_size);
  buffer.beginDraw();
  buffer.noStroke();
  buffer.smooth(1);
  buffer.fill(255,255,255);//canvas background color
  buffer.endDraw();
  selected_type.add("0");
//  println(selected_type);
  reinit(Integer.valueOf(selected_type.get(0)).intValue());
  processImage(sliderValue,Integer.valueOf(selected_type.get(0)).intValue(),0,0,0,255,255,255);
  
  
  //init setup line color (black)
  String int_color = "0";
  line_colorlist.add(int_color);
  line_colorlist.add(int_color);
  line_colorlist.add(int_color);
  
  
  //init setup bg color (white)
  String int_bgcolor = "255";
  bg_colorlist.add(int_bgcolor);
  bg_colorlist.add(int_bgcolor);
  bg_colorlist.add(int_bgcolor);

//  
  slider = new ControlP5(this);
  int myColor = color(150, 177, 208);
  
  slider.addSlider("sliderValue")
    .setLabel("")
    .setRange(0, 3)//0~100の間
    .setValue(0.1)//初期値
    .setSize(110,30)//スライダの大きさ
    .setColorActive(color(109,177,255))//スライダの色
    .setColorBackground(color(191,235,255)) //スライダの背景色 
    .setColorCaptionLabel(color(0)) //キャプションラベルの色
    .setColorForeground(color(109,177,255))  //スライダの色(マウスを離したとき)
    .setColorValueLabel(color(0)) //数値の色
    .setSliderMode(Slider.FIX)
    .setPosition(170, 650)//スライダーの形
    .setNumberOfTickMarks(50);//メモリの値
  
    
    //slider.getController("sliderValue")
    //.getValueLabel();
    
    
    //dropdownlist
    tokens = new String[4];
    tokens[0] = "type1";
    tokens[1] = "type2";
    tokens[2] = "type3";
    tokens[3] = "type4";

    
    List type_list = Arrays.asList(tokens[0],tokens[1],tokens[2],tokens[3]);
    
      slider.addScrollableList("dropdown")
     .setLabel("Pattern Type")
     .setPosition(50, 650)
     .setSize(100, 150)
     .setBarHeight(30)
     .setItemHeight(30)
     .setColorBackground(color(109,177,255))
     .setColorForeground(color(191,235,255))
     .setColorActive(color(191,235,255))
     .addItems(type_list)
     
     .setOpen(false);
     slider.getController("sliderValue")
    .getValueLabel();
    
   //for button
   Button_line = new ControlP5(this);
   Button_line.addButton("line_color")
   .setColorActive(color(191,235,255))
   .setColorForeground(color(191,235,255))
   .setColorBackground(color(109,177,255))
    .setLabel("Line Color")
    .setPosition(300, 650)
    .setSize(100, 30);
   
   
    
   Button_back = new ControlP5(this);
   Button_back.addButton("bg_color")
   .setColorBackground(color(109,177,255))
   .setColorForeground(color(191,235,255))
   .setColorActive(color(191,235,255))
   .setLabel("Background Color")
   .setPosition(420, 650)
   .setSize(100,30);


}

float phase = 0;
int current_step = 0;
String animationKey;

void draw() {
  
  if(animate) {
    println("frame "+(current_step+1)+"/"+frames);
    
    processImage(sliderValue,0,0,0,0,255,255,255);
    
    String fn = foldername + filename + "/frames"+animationKey+"/res_" + animationKey +"_"+(1000+current_step)+"_"+filename+fileext;
    buffer.save(fn);
    
    phase=current_step*TWO_PI/frames;
    
    current_step++;
    if(current_step>=frames) {
      animate = false;
      println("saving frames done");
    
    }
  }
}

void sliderValue(float value){
  processImage(value,Integer.valueOf(selected_type.get(0)),Integer.valueOf(line_colorlist.get(0)).intValue(),Integer.valueOf(line_colorlist.get(1)).intValue(),Integer.valueOf(line_colorlist.get(2)).intValue(),
  Integer.valueOf(bg_colorlist.get(0)),
            Integer.valueOf(bg_colorlist.get(1)),Integer.valueOf(bg_colorlist.get(2)));
}

void dropdown(int selected_type_num) {
  println("drop_seltype"+selected_type_num);
  if (selected_type == null){
    selected_type.add(String.valueOf(selected_type_num));}
  else{selected_type.clear();
  selected_type.add(String.valueOf(selected_type_num));
}
  background(255);
  
  processImage(slider.getController("sliderValue").getValue(),selected_type_num,Integer.valueOf(line_colorlist.get(0)).intValue(),Integer.valueOf(line_colorlist.get(1)).intValue(),Integer.valueOf(line_colorlist.get(2)).intValue(),
  Integer.valueOf(bg_colorlist.get(0)),Integer.valueOf(bg_colorlist.get(1)),Integer.valueOf(bg_colorlist.get(2)));
 
}

void line_color(){
  Color javaColor;
  javaColor  = JColorChooser.showDialog(null,"Java Color Chooser",Color.white);
  JColorChooser chooser = new JColorChooser();
//  chooser.setPreviewPanel(new JPanel());
  chooser.setColor(Color.black);
  Color newColor = chooser.getColor();
//  println(newColor);}
if(javaColor == null) {
            System.out.println("選択されなかったよ(TT)");   
        }else {
            System.out.println("-- 選択された色 --");
            System.out.println("R:" + javaColor.getRed());
            System.out.println("G:" + javaColor.getGreen());
            System.out.println("B:" + javaColor.getBlue());
            String r_color = String.valueOf(javaColor.getRed());
            String g_color = String.valueOf(javaColor.getGreen());
            String b_color = String.valueOf(javaColor.getBlue());
            
            if(line_colorlist==null){
              line_colorlist.add(r_color);
              line_colorlist.add(g_color);
            line_colorlist.add(g_color);}
            
            else{line_colorlist.clear();
            line_colorlist.add(r_color);
            line_colorlist.add(g_color);
            line_colorlist.add(b_color);}
      }
            println("selected_type"+selected_type);
            processImage(slider.getController("sliderValue").getValue(),Integer.valueOf(selected_type.get(0)),Integer.valueOf(line_colorlist.get(0)).intValue(),Integer.valueOf(line_colorlist.get(1)).intValue(),Integer.valueOf(line_colorlist.get(2)).intValue(),
            Integer.valueOf(bg_colorlist.get(0)),Integer.valueOf(bg_colorlist.get(1)),Integer.valueOf(bg_colorlist.get(2)));

            
            
      }

void bg_color(){
  Color javaColor;
  javaColor  = JColorChooser.showDialog(null,"Java Color Chooser",Color.white);
  JColorChooser chooser = new JColorChooser();
//  chooser.setPreviewPanel(new JPanel());
  chooser.setColor(Color.black);
  Color newColor = chooser.getColor();
//  println(newColor);}
if(javaColor == null) {
            System.out.println("選択されなかったよ(TT)");   
        }else {
            System.out.println("-- 選択された色 --");
            System.out.println("R:" + javaColor.getRed());
            System.out.println("G:" + javaColor.getGreen());
            System.out.println("B:" + javaColor.getBlue());
            String r_color = String.valueOf(javaColor.getRed());
            String g_color = String.valueOf(javaColor.getGreen());
            String b_color = String.valueOf(javaColor.getBlue());
            
             if(bg_colorlist==null){
              bg_colorlist.add(r_color);
              bg_colorlist.add(g_color);
            bg_colorlist.add(g_color);}
            
            else{bg_colorlist.clear();
            bg_colorlist.add(r_color);
            bg_colorlist.add(g_color);
            bg_colorlist.add(b_color);}
  
            processImage(slider.getController("sliderValue").getValue(),Integer.valueOf(selected_type.get(0)),Integer.valueOf(line_colorlist.get(0)),
            Integer.valueOf(line_colorlist.get(1)),Integer.valueOf(line_colorlist.get(2)),Integer.valueOf(bg_colorlist.get(0)),
            Integer.valueOf(bg_colorlist.get(1)),Integer.valueOf(bg_colorlist.get(2)));  
            
           
      }
      }





final static int[] non_random_folds = {4,23,25,26,134};

Folds f;
int[] folds;
//boolean stype;
int foldnumber;

void reinit(int selected_type) {
  f = new Folds(width, -rx, rx, -ry, ry);

  foldnumber = (int)floor(random(1))+1;
  //println("foldnumber");
  folds = new int[foldnumber]; 
  for (int i=0;i<foldnumber;i++)
    folds[i] = non_random_folds[selected_type];
}

  

void processImage(float value, int selected_type,int red,int green,int blue, int bg_red, int bg_green, int bg_blue) { //<>//
  buffer.beginDraw();
  
  for (int x=0;x<600;x++) {
    float xx = map(x, 0, buffer.width, -rx, rx);
//    println("PI_X"+xx);
    
    for (int y=0;y<600;y++) {
      float yy = map(y, 0, buffer.height, -ry, ry);

      PVector p = new PVector(xx, yy);
      for (int i=0;i<foldnumber;i++) {
        p = f.getFoldbyNo(selected_type+1, p, value);//call function(number,xxyy,weight)
        //String s=Integer.toString(selected_type);
        //numberlist.add(s);
        
 }
      PVector v = p;
//      println("v.x"+v.x);      
//      println("v.y"+v.y);
      
//      float r = sin(phase+stripe_density*TWO_PI * (stype?(v.y+v.x):(sqrt(sq(v.x)+sq(v.y)))));
      float r = sin(phase+stripe_density*TWO_PI * (v.y+v.x)); //for linear
//      float r = sin(phase+stripe_density*TWO_PI * (sqrt(sq(v.x)+sq(v.y))));//for circular
      
      buffer.fill(bg_red,bg_green,bg_blue);
      buffer.stroke(red, green, blue,map(r, -1, 1, 0, 255));
      buffer.rect(x, y, 1, 1);
    }
  }

  buffer.endDraw();
  image(buffer, 50,30, 600, 600);
  
}


void keyPressed() {
  // SPACE to save
  if (keyCode == 32) {
//      String nlist = namelist.get(namelist.size() - 1);
////      String slist = stypelist.get(stypelist.size() - 1);
//      String numlist = numberlist.get(numberlist.size() - 1);
//      String fn =  foldername + filename +"/"+ numlist+"_"+nlist+"_linear"+fileext; // change name (linear or circular)
        
        String fn = "/Users/natsumull/Research/Innovation/proj_app/opf_app/three-projected-material-master/examples/images/test.png";
    buffer.save(fn);
    println("Image "+ fn + " saved");
  } else if(key == 'a') {
    phase = 0;
    current_step = 0;
    animate = true;
    animationKey = sessionid + hex((int)random(0xffff), 4);
    println("animate frames");
  }
}

